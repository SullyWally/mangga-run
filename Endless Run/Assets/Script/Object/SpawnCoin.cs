using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoin : MonoBehaviour
{
    [SerializeField] private GameObject prefabSpawn;
    [SerializeField] private GameObject[] spawnPoint;

    [SerializeField] private float coinSpawned;

    [SerializeField] private float coinInterval;
    private int coinPosition;
    // Start is called before the first frame update
    void Start()
    {
        coinInterval = 0.5f;
        coinPosition = Random.Range(0, 2);
        InvokeRepeating("CoinSpawned", coinInterval, coinInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CoinSpawned()
    {
        coinSpawned += 1;
        int changePosition = Random.Range(0, 7);
        if(coinSpawned % changePosition == 0)
        {
            coinPosition += 1;
            if(coinPosition >= 3)
            {
                coinPosition = Random.Range(0, 1);
            }
            
            
            
        }
        if(coinSpawned % 7 == 0)
        {
            if (coinInterval > 0.2f)
            {
                coinInterval -= 0.1f;
            }
            else if(coinInterval >0.01f)
            {
                coinInterval -= 0.01f;
            }

        }

        Instantiate(prefabSpawn, spawnPoint[coinPosition].transform.position, Quaternion.identity);
    }
}
