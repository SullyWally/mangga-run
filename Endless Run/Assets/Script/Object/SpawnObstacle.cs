using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacle : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabSpawn;
    [SerializeField] private GameObject[] spawnPoint;

    [SerializeField] private float spawnInterval;

    [SerializeField] private float objectSpawned;
   

    

    private float _time;

    // Start is called before the first frame update
    void Start()
    {
       
        InvokeRepeating("SpawnObjects", spawnInterval, spawnInterval);
        
    }

    void Update()
    {
        
    }

    void SpawnObjects()
    {
        objectSpawned += 1;
        int randomspawn = Random.Range(0, 2);
        int randomspawn2 = Random.Range(0, 2);
        int randomObjectSpawn = Random.Range(0, 3);
        Vector3 objectRotation = prefabSpawn[randomObjectSpawn].transform.eulerAngles;
        Instantiate(prefabSpawn[randomObjectSpawn], spawnPoint[randomspawn].transform.position,Quaternion.Euler(objectRotation));
        
        if(spawnInterval <= 1f)
        {
            if(randomspawn2 != randomspawn && objectSpawned % 3 == 0)
            {
                randomObjectSpawn = Random.Range(0, 1);
                Instantiate(prefabSpawn[randomObjectSpawn], spawnPoint[randomspawn2].transform.position, Quaternion.Euler(objectRotation));
            }
        }

        if(objectSpawned % 5 == 0)
        {
            if(spawnInterval > 0.6f)
                spawnInterval -= 0.2f;
        }

    }
   
}
