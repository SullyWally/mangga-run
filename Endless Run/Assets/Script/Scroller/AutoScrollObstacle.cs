using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScrollObstacle : MonoBehaviour
{
    private SpeedManager speedObject;

    // Start is called before the first frame update
    void Start()
    {
        speedObject = GameObject.Find("SpeedHandler").GetComponent<SpeedManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 0, speedObject.speed);

        if (transform.position.z <= -27.5f)
        {
            Destroy(this.gameObject);
        }
    }
}
