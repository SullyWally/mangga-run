using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScroll : MonoBehaviour
{
    private SpeedManager speedObject;

    void Start()
    {
        speedObject = GameObject.Find("SpeedHandler").GetComponent<SpeedManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 0, speedObject.speed);
       
        if(transform.position.z <= -27.5f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 35f);
        }

    }
        
}
