using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    [SerializeField] private string sceneName;

    public void MoveScene()
    {
        SceneManager.LoadScene(sceneName);
    }
}
