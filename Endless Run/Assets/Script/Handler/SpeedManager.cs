using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedManager : MonoBehaviour
{
    [SerializeField] private GameObject player;

    public float speed;
    public int coin;

    // Start is called before the first frame update
    void Start()
    {
        speed = -0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (coin == 10)
        {
            coin = 0;
            if(speed > -0.2f)
                speed -= 0.02f;
        }
    }
}
