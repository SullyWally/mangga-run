using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreHandler : MonoBehaviour
{
    public int highScore;
    [SerializeField] private Text highScoreText;
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        highScore = PlayerPrefs.GetInt("highscore");
        score = PlayerPrefs.GetInt("score");
        if(highScore <= score)
        {
            highScore = score;
            PlayerPrefs.SetInt("highscore", highScore);
        }
        highScoreText.text = "High Score : " + highScore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
