using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
    [SerializeField] private PlayerLives player;

    private Vector3 originalPos;
    public bool returning;

    // Start is called before the first frame update
    void Start()
    {
        returning = false;
        originalPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (returning)
        {
            transform.localPosition += new Vector3(0, 0, -0.01f);
            if (transform.localPosition.z <= -3.6f)
            {
                player.lives += 1;
                returning = false;
            }
        }
    }

    public void Move()
    {
        transform.localPosition = new Vector3(0, 0, -2.5f);
        StartCoroutine("MoveBackToPos");
    }

    IEnumerator MoveBackToPos()
    {
        yield return new WaitForSeconds(20f);
        returning = true;
        StopCoroutine("MoveBackToPos");
    }
}
