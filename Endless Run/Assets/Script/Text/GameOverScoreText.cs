using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScoreText : MonoBehaviour
{
    [SerializeField] private int playerScore;
    [SerializeField] private Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        playerScore = PlayerPrefs.GetInt("score");
        scoreText.text = "Score : " + playerScore; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
