using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveLane : MonoBehaviour
{
    [SerializeField] private GameObject[] lane;

    private int currentLane;

    // Start is called before the first frame update
    void Start()
    {
        currentLane = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("left"))
        {
            if(currentLane != 0)
            {
                currentLane -= 1;
                transform.position = lane[currentLane].transform.position;
            }
        }

        if (Input.GetKeyDown("right"))
        {
            if (currentLane != 2)
            {
                currentLane += 1;
                transform.position = lane[currentLane].transform.position;
            }
        }

        
    }


}
