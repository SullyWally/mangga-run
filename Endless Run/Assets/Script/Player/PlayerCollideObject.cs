using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollideObject : MonoBehaviour
{
    [SerializeField] private GameObject gameSpeed;
    [SerializeField] private PlayerLives player;
    public int coinGained;

    [SerializeField] private GameObject enemy;

    [SerializeField] private AudioSource coinSFX;
    [SerializeField] private AudioSource hitSFX;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            gameSpeed.GetComponent<SpeedManager>().speed = -0.1f;
            gameSpeed.GetComponent<SpeedManager>().coin = 0;
            player.lives -= 1;
            Destroy(other.gameObject);
            enemy.GetComponent<MoveEnemy>().Move();
            hitSFX.Play();
        }
        if (other.gameObject.tag == "Coin")
        {
            gameSpeed.GetComponent<SpeedManager>().coin += 1;
            coinGained += 1;
            Destroy(other.gameObject);
            coinSFX.Play();
        }
    }

    
}
