using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLives : MonoBehaviour
{
    public int lives;

    // Start is called before the first frame update
    void Start()
    {
        lives = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if(lives == 0)
        {
            PlayerPrefs.SetInt("score", GetComponent<PlayerCollideObject>().coinGained);
            SceneManager.LoadScene("GameOver");
        }
        
    }
}
